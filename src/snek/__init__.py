#!/usr/bin/env python3

"""
Print an ASCII Snek.

Usage:
    snek [--type=TYPE]

"""
import docopt
import pkg_resources


def get_sneks():
    sneks = {}
    for entry_point in pkg_resources.iter_entry_points('snek_types'):
        sneks[entry_point.name] = entry_point.load()
    return sneks


def main():
    args = docopt.docopt(__doc__)
    snek_type = args['--type'] or 'normal'
    print(get_sneks()[snek_type])

if __name__ == '__main__':
    main()
