# Python Entrypoints Snek

This project is a tutorial for entrypoints in Python based on
[Python Entry Points Explained](https://amir.rachum.com/python-entry-points/).
This project uses the modern `pyproject.toml` configuraiton file instead of
`setup.py`.

## Installation

```shell
python3 -m pip install .
```

## Normal Snek

```shell
snek  # short for `snek --type normal`
```

```log
    --..,_                     _,.--.
       `'.'.                .'`__ o  `;__.
          '.'.            .'.'`  '---'`  `
            '.`'--....--'`.'
              `'--....--'`
```

## Fancy Snek

```shell
snek --type fancy
```

```log
                          _,..,,,_
                     '``````^~"-,_`"-,_
       .-~c~-.                    `~:. ^-.
   `~~~-.c    ;                      `:.  `-,     _.-~~^^~:.
         `.   ;      _,--~~~~-._       `:.   ~. .~          `.
          .` ;'   .:`           `:       `:.   `    _.:-,.    `.
        .' .:   :'    _.-~^~-.    `.       `..'   .:      `.    '
       :  .' _:'   .-'        `.    :.     .:   .'`.        :    ;
       :  `-'   .:'             `.    `^~~^`   .:.  `.      ;    ;
        `-.__,-~                  ~-.        ,' ':    '.__.`    :'
                                     ~--..--'     ':.         .:'
                                                     ':..___.:'
```
